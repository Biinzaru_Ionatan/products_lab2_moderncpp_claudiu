#include <cstdint>
#include <string>
#include "IPriceable.h"
#include "Product.h"

class PerishableProduct : public Product
{
public:
	PerishableProduct(const int32_t& id, const std::string& name, const float& rawPrice, const std::string& m_expirationDate);
	std::string GetExpirationDate() const;
	int32_t GetVAT() const;
	float GetPrice() const;

private:
	std::string m_expirationDate;
	const int32_t m_VAT = 9;
};

