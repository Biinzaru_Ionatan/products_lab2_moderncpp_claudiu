#include "PerishableProduct.h"

PerishableProduct::PerishableProduct(const int32_t& id, const std::string& name, const float& rawPrice, const std::string& expirationDate) :
	Product(id, name, rawPrice), m_expirationDate(expirationDate)
{
	// empty
}

std::string PerishableProduct::GetExpirationDate() const
{
	return this->m_expirationDate;
}

int32_t PerishableProduct::GetVAT() const
{
	return this->m_VAT;
}

float PerishableProduct::GetPrice() const
{
	return static_cast<float>(this->m_rawPrice + this->m_VAT * 0.01 * this->m_rawPrice);
}
