#pragma once
#include <cstdint>
#include <string>
#include "IPriceable.h"
#include "Product.h"
#include "NonperishableProductType.h"

class NonperishableProduct : public Product
{
public:
	NonperishableProduct(const int32_t& id, const std::string& name, const float& rawPrice, NonperishableProductType& type);
	NonperishableProductType GetType() const;
	int32_t GetVAT() const;
	float GetPrice() const;

private:
	NonperishableProductType m_type;
	const int32_t m_VAT = 19;
};

